package dao;

import dao.UserDAO;
import db.DBConnection;

import junit.framework.Assert;
import model.User;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/*
 * @author tyrelsouza, @date 2/27/17 9:23 PM
 */
public class UserDAOTest {
    UserDAO userDao;

    @Before
    public void setUp() throws Exception {
        userDao = new UserDAO();
    }

    @Test
    public void testUserDAO(){
        User user = userDao.getById(1);
        Assert.assertEquals("tyrel", user.getUsername());
    }

    @Test
    public void testUserDAOgetAll(){
        ArrayList<User> users = userDao.findAll();
        Assert.assertEquals("tyrel", users.get(0).getUsername());
    }

}
