
import junit.framework.Assert;
import model.User;
import org.junit.Test;

/*
 * @author tyrelsouza, @date 2/27/17 9:23 PM
 */
public class HourTrackerTest {
    @Test
    public void testUserCreate() {
        User user = new User(1, "tyrel", "Tyrel Souza");
        // assertTrue("someLibraryMethod should return 'true'", classUnderTest.someLibraryMethod());

        // Basic Tests for Basic Getters and setters
        Assert.assertEquals(1, user.getId());
        Assert.assertEquals("tyrel", user.getUsername());
        Assert.assertEquals("Tyrel Souza", user.getFullName());
    }
}
