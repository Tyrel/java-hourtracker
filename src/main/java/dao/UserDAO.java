package dao;

import db.DBConnection;
import model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tyrelsouza on 2/27/17.
 CREATE TABLE user (
 `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 `username` varchar(100) not null,
 `full_name` varchar(200) not null
 );
 */
public class UserDAO {
    DBConnection db;
    public UserDAO() {
        db = new DBConnection();
    }
    private User getUserFromResultSet(ResultSet rs){
        try {
            Integer userid = Integer.parseInt(rs.getString("id"));
            String username = rs.getString("username");
            String fullName = rs.getString("full_name");
            User user = new User(userid, username, fullName);
            return user;
        } catch (SQLException e) {
            return null;
        }
    }

    public ArrayList<User> findAll(){
        ArrayList<User> users = new ArrayList<User>();
        PreparedStatement ps;
        try {
            ps = db.connection.prepareStatement(
                    "SELECT id, username, full_name FROM user");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                users.add(getUserFromResultSet(rs));
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public User getById(int id){
        PreparedStatement ps;
        try {
            ps = db.connection.prepareStatement(
                    "SELECT id, username, full_name FROM user where id = ? LIMIT 1");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            rs.next();

            return getUserFromResultSet(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean insertUser(User User){
        return false;
    }
    public boolean updateUser(User User){
        return false;
    }
    public boolean deleteUser(User User){
        return false;

    }
}
